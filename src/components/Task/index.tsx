import { FC } from 'react';
import { useDispatch } from 'react-redux';
import { taskActions } from '../../lib/redux/actions/task';
import { ITask } from '../types';

export const Task: FC<IPropTypes> = (props) => {
    const dispatch = useDispatch();

    const OnOpenTask = (task: ITask) => {
        dispatch(taskActions.setOpenTask(task));
    };

    const formattedDate = (date: string): string => {
        return `${new Date(date).getUTCDate()} ${new Date(date).toLocaleString('default', { month: 'short' })} ${new Date(date).getFullYear()} `;
    };

    return (
        <div className = { `task ${props.completed ? 'completed' : ''}` } onClick = { () => OnOpenTask(props) }>
            <span className = 'title'>{ props.title }</span>
            <div className = 'meta'>
                <span className = 'deadline'>{ formattedDate(props.deadline) }</span>
                <span
                    className = 'tag'
                    style = { { color: props.tag.color, background: props.tag.bg } }>
                    { props.tag.name }
                </span>
            </div>
        </div>
    );
};

interface IPropTypes extends ITask{}
