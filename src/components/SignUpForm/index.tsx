import { FC, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';

import { ISignUpForm } from '../types';
import { schema } from './config';
import { authActions } from '../../lib/redux/actions/auth';
import { getToken } from '../../lib/redux/selectors/auth';

export const SignUpForm: FC = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const token = useSelector(getToken);

    const form = useForm<ISignUpForm>({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    useEffect(() => {
        if (token) {
            navigate('/todo/task-manager');
        }
    }, [token]);

    const onSubmit = form.handleSubmit((newUserData: ISignUpForm) => {
        const { confirmPassword, ...newUser } = newUserData;
        dispatch(authActions.fetchSignUpAsync(newUser));
    });

    return (
        <section className = 'publish-tip sign-form'>
            <form onSubmit = { onSubmit }>
                <fieldset>
                    <legend>Регистрация</legend>
                    <label className = 'label'>
                        <span className = 'errorMessage'>{ form.formState.errors.name?.message }</span>
                        <input
                            placeholder = 'Имя и фамилия'
                            type = 'text'
                            { ...form.register('name') } />
                    </label>
                    <label className = 'label'>
                        <span className = 'errorMessage'>{ form.formState.errors.email?.message }</span>
                        <input
                            placeholder = 'Электропочта'
                            type = 'text'
                            { ...form.register('email') } />
                    </label>
                    <label className = 'label'>
                        <span className = 'errorMessage'>{ form.formState.errors.password?.message }</span>
                        <input
                            placeholder = 'Пароль'
                            type = 'password'
                            { ...form.register('password') } />
                    </label>
                    <label className = 'label'>
                        <span className = 'errorMessage'>{ form.formState.errors.confirmPassword?.message }</span>
                        <input
                            placeholder = 'Подтверждение пароля'
                            type = 'password'
                            { ...form.register('confirmPassword') } />
                    </label>
                    <input
                        className = 'button-login'
                        type = 'submit'
                        value = 'Зарегистрироваться' />
                </fieldset>
                <p>Перейти к <Link to = '/todo/login'>логину</Link>.</p>
            </form>
        </section>
    );
};
