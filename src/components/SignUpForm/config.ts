import * as yup from 'yup';
import { ISignUpForm } from '../types';

// eslint-disable-next-line no-template-curly-in-string
const tooShortMessage = 'минимальная длина - ${min} символов';

export const schema: yup.SchemaOf<ISignUpForm> = yup.object().shape({
    name: yup
        .string()
        .min(2, tooShortMessage)
        .required('*'),
    email: yup
        .string()
        .email()
        .required('Поле email обязательно для заполнения'),
    password: yup
        .string()
        .min(8, tooShortMessage)
        .required('*'),
    confirmPassword: yup
        .string()
        .oneOf([yup.ref('password'), 'Пароли должны совпадать'])
        .required('*'),
});
