import { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink, useNavigate } from 'react-router-dom';
import { authActions } from '../../lib/redux/actions/auth';
import { getToken } from '../../lib/redux/selectors/auth';

export const Navigation: FC = () => {
    const isUserToken = useSelector(getToken);
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const OnLogout = () => {
        dispatch(authActions.fetchLogoutAsync());
    };

    useEffect(() => {
        const localToken = localStorage.getItem('token');
        if (localToken) {
            dispatch(authActions.setToken(localToken));
        } else if (!isUserToken) {
            navigate('/todo/login');
        }
    }, [isUserToken]);

    return (
        <nav>
            { !isUserToken && <NavLink to = '/todo/login'>Войти</NavLink> }
            <NavLink to = '/todo/task-manager'>К задачам</NavLink>
            <NavLink to = '/todo/profile'>Профиль</NavLink>
            { isUserToken && <button className = 'button-logout' onClick = { OnLogout }>Выйти</button> }
        </nav>
    );
};
