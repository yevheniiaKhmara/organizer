import { FC } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { tagsActions } from '../../lib/redux/actions/tags';
import { getSelectedTagId } from '../../lib/redux/selectors/tags';
import { ITag } from '../types';

export const Tag: FC<IPropTypes> = (props) => {
    const selectedTagId = useSelector(getSelectedTagId);
    const dispatch = useDispatch();

    const onSelectedTagId = (tagId: string) => {
        // eslint-disable-next-line no-unneeded-ternary
        dispatch(tagsActions.setSelectedTagId(tagId ? tagId : selectedTagId));
    };

    return (
        <span
            className = { `tag ${props.id === selectedTagId ? 'selected' : ''}` }
            style = { { color: props.color, background: props.bg  } }
            onClick = { () => onSelectedTagId(props.id) }>
            { props.name }
        </span>
    );
};

interface IPropTypes extends ITag {}
