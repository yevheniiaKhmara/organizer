import { yupResolver } from '@hookform/resolvers/yup';
import { FC, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import DatePicker, { registerLocale } from 'react-datepicker';
import ru from 'date-fns/locale/ru';

import { tagsActions } from '../../lib/redux/actions/tags';
import { getSelectedTagId, getTags } from '../../lib/redux/selectors/tags';
import { getOpenTask, getClickedNewTask } from '../../lib/redux/selectors/task';
import { Tag } from '../Tag';
import { INewTask, ITag, ITask } from '../types';
import { schema } from './config';
import { taskActions } from '../../lib/redux/actions/task';

registerLocale('ru', ru);

export const TaskCard: FC = () => {
    const tags = useSelector(getTags);
    const selectedTagId = useSelector(getSelectedTagId);
    const isOpenNewTask = useSelector(getClickedNewTask);
    const openedTask = useSelector(getOpenTask);
    let newTaskData = null;
    const dispatch = useDispatch();
    const [startDate, setStartDate] = useState(new Date());
    const [isDataChanged, setIsDataChanged] = useState(false);


    const form = useForm<INewTask>({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    useEffect(() => {
        if (openedTask) {
            form.setValue('title', openedTask.title);
            form.setValue('deadline', openedTask.deadline);
            form.setValue('description', openedTask.description);
            dispatch(tagsActions.setSelectedTagId(openedTask.tag.id));
        } else {
            form.reset({
                title:       '',
                description: '',
                deadline:    startDate.toISOString(),
                completed:   false,
                tag:         selectedTagId,
            });
        }
    }, [openedTask]);

    const onReset = () => {
        form.reset();
    };

    const tagsJSX = tags && tags?.map((tag: ITag) => {
        return <Tag key = { tag.id } { ...tag } />;
    });

    const onSaveNewTask = form.handleSubmit((newTask) => {
        newTaskData = {
            ...newTask,
            deadline:  startDate.toISOString(),
            tag:       selectedTagId,
            completed: false,
        };

        // eslint-disable-next-line @typescript-eslint/no-unused-expressions
        isOpenNewTask
            ? dispatch(taskActions.createTaskAsync(newTaskData))
            : dispatch(taskActions.updateTaskAsync(openedTask.id, newTaskData));
    });

    const OnDeleteTask = (id: string) => {
        dispatch(taskActions.deleteTaskAsync(id));
    };

    const OnCompletedTask = (task: ITask) => {
        const completedTask = {
            completed:   true,
            title:       task.title,
            description: task.description,
            deadline:    startDate.toISOString(),
            tag:         selectedTagId,
        };
        dispatch(taskActions.updateTaskAsync(task.id, completedTask));
    };

    useEffect(() => {
        const defaultData = openedTask ? { ...openedTask } : {
            title:       '',
            description: '',
            deadline:    startDate.toISOString(),
            completed:   false,
            tag:         selectedTagId,
        };

        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const subscription = form.watch((value, { name, type }) => {
            const isChanged = defaultData.title !== value.title
                || defaultData.description !== value.description
                || defaultData.deadline !== value.deadline;

            setIsDataChanged(isChanged);
        });

        return () => subscription.unsubscribe();
    }, [form.watch, openedTask]);

    return (
        <div className = 'task-card'>
            <form onSubmit = { onSaveNewTask }>
                { !isOpenNewTask
                    ? <div className = 'head'>
                        <button
                            type = 'button'
                            className = 'button-complete-task'
                            onClick = { () => OnCompletedTask(openedTask) }>завершить</button>
                        <div className = 'button-remove-task' onClick = { () => OnDeleteTask(openedTask.id) }></div>
                    </div>
                    : <div className = 'head'></div>
                }
                <div className = 'content'>
                    <label className = 'label'>Задачи
                        <input
                            className = 'title'
                            placeholder = 'Пройти интенсив по React + Redux + TS + Mobx'
                            type = 'text'
                            { ...form.register('title') } />
                    </label>
                    <div className = 'deadline'>
                        <span className = 'label'>Дедлайн</span>
                        <span className = 'date'>
                            <div className = 'react-datepicker-wrapper'>
                                <div className = 'react-datepicker__input-container'>
                                    <DatePicker
                                        locale = 'ru'
                                        dateFormat = 'dd MMM yyyy'
                                        selected = { startDate }
                                        minDate = { new Date() }
                                        onChange = { (date: Date) => {
                                            form.setValue('deadline', date.toISOString());
                                            setStartDate(date);
                                        } } />
                                </div>
                            </div>
                        </span>
                    </div>
                    <div className = 'description'>
                        <label className = 'label'>Описание
                            <textarea
                                className = 'text'
                                placeholder = 'После изучения всех технологий, завершить работу над проектами и найти работу.'
                                { ...form.register('description') }>
                            </textarea>
                        </label>
                    </div>
                    <div className = 'tags'>
                        { tagsJSX }
                    </div>
                    <div className = 'errors'>
                        <p className = 'errorMessage'>{ form.formState.errors.title?.message || form.formState.errors.description?.message }</p>
                    </div>
                    <div className = 'form-controls'>
                        <button
                            type = 'reset'
                            className = 'button-reset-task'
                            onClick = { onReset }
                            disabled = { !isDataChanged }>
                            Reset
                        </button>
                        <button
                            type = 'submit'
                            className = 'button-save-task'
                            disabled = { !isDataChanged }>Save</button>
                    </div>
                </div>
            </form>
        </div>
    );
};
