import * as yup from 'yup';
import { INewTask } from '../types';

// eslint-disable-next-line no-template-curly-in-string
const tooShortMessage = 'минимальная длина ${path}  - ${min} символов';

export const schema: yup.SchemaOf<INewTask> = yup.object().shape({
    title: yup
        .string()
        .min(3, tooShortMessage)
        .required(),
    deadline: yup
        .string(),
    description: yup
        .string()
        .min(3, tooShortMessage)
        .required(),
    tag: yup
        .string(),
    completed: yup
        .boolean(),
});
