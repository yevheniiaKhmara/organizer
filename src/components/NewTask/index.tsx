import { FC } from 'react';
import { useDispatch } from 'react-redux';
import { taskActions } from '../../lib/redux/actions/task';

export const NewTask: FC = () => {
    const dispatch = useDispatch();
    const openNewTask = () => {
        dispatch(taskActions.setClickedNewTask(true));
    };

    return (
        <div className = 'controls'>
            <i className = 'las'></i>
            <button className = 'button-create-task' onClick = { openNewTask }>Новая задача</button>
        </div>
    );
};
