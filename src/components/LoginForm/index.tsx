import { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';

import { ILoginForm } from '../types';
import { schema } from './config';
import { authActions } from '../../lib/redux/actions/auth';
import { getToken } from '../../lib/redux/selectors/auth';

export const LoginForm: FC = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const token = useSelector(getToken);

    useEffect(() => {
        if (token) {
            navigate('/todo/task-manager');
        }
    }, [token]);

    const form = useForm<ILoginForm>({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const onSubmit = form.handleSubmit((user: ILoginForm) => {
        dispatch(authActions.fetchLoginAsync(user));
    });

    return (
        <section className = 'sign-form'>
            <form onSubmit = { onSubmit }>
                <fieldset>
                    <legend>Вход</legend>
                    <label className = 'label'>
                        <span className = 'errorMessage'>{ form.formState.errors.email?.message }</span>
                        <input
                            placeholder = 'Электропочта'
                            type = 'text'
                            { ...form.register('email') } />
                    </label>
                    <label className = 'label'>
                        <span className = 'errorMessage'>{ form.formState.errors.password?.message }</span>
                        <input
                            placeholder = 'Пароль'
                            type = 'password'
                            { ...form.register('password') } />
                    </label>
                    <input
                        className = 'button-login'
                        type = 'submit'
                        value = 'Войти' />
                </fieldset>
                <p>Если у вас до сих пор нет учётной записи, вы можете <Link to = '/todo/signup'>зарегистрироваться</Link>.</p>
            </form>
        </section>
    );
};
