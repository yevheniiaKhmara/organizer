import * as yup from 'yup';
import { ILoginForm } from '../types';

// eslint-disable-next-line no-template-curly-in-string
const tooShortMessage = 'минимальная длина - ${min} символов';

export const schema: yup.SchemaOf<ILoginForm> = yup.object().shape({
    email: yup
        .string()
        .email()
        .min(2, 'почта должна быть настоящей')
        .required('Поле email обязательно для заполнения'),
    password: yup
        .string()
        .min(8, tooShortMessage)
        .required('*'),
});
