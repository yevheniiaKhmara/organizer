export interface ISignUpForm {
    name: string;
    email: string;
    password: string;
    confirmPassword: string;
}

export interface ISignUp extends Omit<ISignUpForm, 'confirmPassword'>{}

export interface ILoginForm extends Omit<ISignUpForm, 'confirmPassword' | 'name'>{}

export interface IAuthWithToken {
    data: string;
}

export interface INewTask {
    title: string;
    deadline?: string;
    description: string;
    tag?: string;
    completed?: boolean;
}

export interface ITag {
    id: string;
    name: string;
    color: string;
    bg: string;
}

export interface ITask {
    id: string;
    title: string;
    deadline: string;
    created: string;
    description: string;
    completed: boolean;
    tag: ITag;
}

export interface IProfile {
    created: string;
    email: string;
    id: string;
    name: string;
}
