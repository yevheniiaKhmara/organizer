import { FC } from 'react';
import { useSelector } from 'react-redux';
import {
    getClickedNewTask, getTasks, getOpenTask,
} from '../../lib/redux/selectors/task';
import { Task } from '../Task';
import { TaskCard } from '../TaskCard';
import { ITask } from '../types';

export const TaskList: FC = () => {
    const openTask = useSelector(getOpenTask);
    const isOpenNewTask = useSelector(getClickedNewTask);
    const tasks = useSelector(getTasks);

    const tasksJSX = tasks?.map((task: ITask) => {
        return <Task key = { task.id } { ...task } />;
    });

    return (
        <div className = 'wrap'>
            <div className = { `list ${tasks?.length ? '' : 'empty'}` }>
                <div className = 'tasks'>
                    { tasksJSX }
                </div>
            </div>
            { (openTask || isOpenNewTask) && <TaskCard /> }
        </div>
    );
};
