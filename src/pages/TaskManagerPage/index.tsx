import { FC, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { api } from '../../api';
import { NewTask } from '../../components/NewTask';
import { TaskList } from '../../components/TaskList';
import { authActions } from '../../lib/redux/actions/auth';
import { tagsActions } from '../../lib/redux/actions/tags';
import { taskActions } from '../../lib/redux/actions/task';

export const TaskManagerPage: FC = () => {
    const dispatch = useDispatch();

    const getProfile = async () => {
        const profile = await api.getProfile();
        dispatch(authActions.setProfile(profile));
    };

    useEffect(() => {
        dispatch(tagsActions.fetchTagsAsync());
    }, []);

    useEffect(() => {
        dispatch(taskActions.fetchTasksAsync());
    }, []);

    useEffect(() => {
        void getProfile();
    }, []);

    return (
        <>
            <NewTask />
            <TaskList />
        </>

    );
};
