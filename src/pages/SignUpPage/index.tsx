import { FC } from 'react';
import { SignUpForm } from '../../components/SignUpForm';

export const SignUpPage: FC = () => {
    return (
        <SignUpForm />
    );
};
