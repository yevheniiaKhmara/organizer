import { ITask } from '../../../components/types';
import { RootState } from '../init/store';

export const getClickedNewTask = (state: RootState): boolean => {
    return state.task.isOpenNewTask;
};

export const getTasks = (state: RootState): ITask[] => {
    return state.task.tasks;
};

export const getOpenTask = (state: RootState): ITask => {
    return state.task.task;
};

export const getTasksAsync = (state: RootState): ITask[] => {
    return state.task.tasks;
};

