import { IProfile } from '../../../components/types';
import { RootState } from '../init/store';

export const getToken = (state: RootState): string => {
    return state.auth.token;
};

export const getProfile = (state: RootState): IProfile => {
    return state.auth.profile;
};
