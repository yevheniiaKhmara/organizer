import { ITag } from '../../../components/types';
import { RootState } from '../init/store';

export const getTags = (state: RootState): ITag[] => {
    return state.tags.tags;
};

export const getSelectedTagId = (state: RootState): string => {
    return state.tags.tagId;
};
