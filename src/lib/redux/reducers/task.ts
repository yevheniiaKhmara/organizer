import { AnyAction } from 'redux';
import { ITask } from '../../../components/types';
import { taskTypes } from '../types/task';

interface ITaskState {
    isOpenNewTask: boolean;
    tasks:   ITask[],
    task:               ITask | null,
    completedTask:      boolean;
}

const initialState: ITaskState = {
    isOpenNewTask: false,
    tasks:         [],
    task:          null,
    completedTask: false,
};

// eslint-disable-next-line default-param-last
export const taskReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case taskTypes.CLICKED_NEW_TASK: {
            return {
                ...state,
                isOpenNewTask: true,
                task:          null,
            };
        }

        case taskTypes.SET_OPEN_TASK: {
            return {
                ...state,
                isOpenNewTask: false,
                task:          action.payload,
            };
        }

        case taskTypes.GET_TASKS: {
            return {
                ...state,
                tasks: action.payload,
            };
        }

        case taskTypes.CREATE_TASK: {
            return {
                ...state,
                isOpenNewTask: false,
                task:          null,
                tasks:         [action.payload, ...state.tasks],
            };
        }

        case taskTypes.UPDATE_TASK: {
            return {
                ...state,
                isOpenNewTask: false,
                task:          null,
                tasks:         state.tasks.map((task) => {
                    if (task.id === action.payload.id) {
                        return action.payload;
                    }

                    return task;
                }),
            };
        }

        case taskTypes.DELETE_TASK: {
            return {
                ...state,
                isOpenNewTask: false,
                task:          null,
                tasks:         state.tasks.filter((task) => task.id !== action.payload),
            };
        }

        default: {
            return state;
        }
    }
};
