import { AnyAction } from 'redux';
import { tagsTypes } from '../types/tags';

const initialState = {
    tags:  [],
    tagId: '',
};

// eslint-disable-next-line default-param-last
export const tagsReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case tagsTypes.SET_SELECTED_TAG_ID: {
            return {
                ...state,
                tagId: action.payload,
            };
        }

        case tagsTypes.GET_TAGS: {
            return {
                ...state,
                tags:  action.payload,
                tagId: action.payload[ 0 ].id || '',
            };
        }

        default: {
            return state;
        }
    }
};
