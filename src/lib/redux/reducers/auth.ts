import { AnyAction } from 'redux';
import { authTypes } from '../types/auth';

const initialState = {
    token:        '',
    errorMessage: '',
    error:        false,
    profile:      null,
};

// eslint-disable-next-line default-param-last
export const authReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case authTypes.SET_TOKEN: {
            return {
                ...state,
                token:        action.payload,
                error:        false,
                errorMessage: '',
            };
        }

        case authTypes.RESET_ERROR: {
            return {
                ...state,
                error:        false,
                errorMessage: '',
            };
        }

        case authTypes.SET_ERROR: {
            return {
                ...state,
                error:        true,
                errorMessage: action.payload,
            };
        }

        case authTypes.SET_PROFILE: {
            return {
                ...state,
                error:   false,
                profile: action.payload,
            };
        }

        default: {
            return state;
        }
    }
};
