import { toast } from 'react-toastify';
import { api } from '../../../api';
import { ILoginForm, IProfile, ISignUp } from '../../../components/types';
import { AppThunk } from '../init/store';
import { authTypes } from '../types/auth';


export const authActions = Object.freeze({
    resetError: () => {
        return {
            type: authTypes.RESET_ERROR,
        };
    },

    setError: (message: string) => {
        return {
            type:    authTypes.SET_ERROR,
            error:   true,
            payload: message,
        };
    },

    setToken: (token: string) => {
        return {
            type:    authTypes.SET_TOKEN,
            payload: token,
        };
    },

    setProfile: (profile: IProfile) => {
        return {
            type:    authTypes.SET_PROFILE,
            payload: profile,
        };
    },

    fetchLoginAsync: (user: ILoginForm): AppThunk => async (dispatch) => {
        try {
            const token = await api.login(user);
            dispatch(authActions.setToken(token.data));
            localStorage.setItem('token', token?.data);
            toast.success('Добро пожаловать', {
                position: toast.POSITION.TOP_RIGHT,
            });
        } catch (error) {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const { response } = error as any;
            dispatch(authActions.setError(response.data.message));
            toast.error(response.data.message, {
                position: toast.POSITION.TOP_RIGHT,
            });
        }
    },

    fetchSignUpAsync: (newUserData: ISignUp): AppThunk => async (dispatch) => {
        try {
            const token = await api.signup(newUserData);
            dispatch(authActions.setToken(token.data));
            localStorage.setItem('token', token?.data);
            const profile = await api.getProfile();
            console.log('profile', profile);

            toast.success(`Добро пожаловать, ${profile?.name}`, {
                position: toast.POSITION.TOP_RIGHT,
            });
        } catch (error) {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const { response } = error as any;
            dispatch(authActions.setError(response.data.message));
            toast.error(response.data.message, {
                position: toast.POSITION.TOP_RIGHT,
            });
        }
    },

    fetchLogoutAsync: (): AppThunk => async (dispatch) => {
        try {
            await api.logout();
            localStorage.removeItem('token');
            dispatch(authActions.setToken(''));
            toast.info('Возвращайтесь поскорее ;) Мы будем скучать.', {
                position: 'top-right',
            });
        } catch (error) {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const { response } = error as any;
            dispatch(authActions.setError(response.data.message));
            toast.error(response.data.message, {
                position: toast.POSITION.TOP_RIGHT,
            });
        }
    },
});

