import { toast } from 'react-toastify';
import { api } from '../../../api';
import { ITag } from '../../../components/types';
import { AppThunk } from '../init/store';
import { tagsTypes } from '../types/tags';
import { authActions } from './auth';
import { taskActions } from './task';


export const tagsActions = Object.freeze({
    setSelectedTagId: (tagId: string) => {
        return {
            type:    tagsTypes.SET_SELECTED_TAG_ID,
            payload: tagId,
        };
    },

    startFetching: () => {
        return {
            type: tagsTypes.START_FETCHING,
        };
    },

    stopFetching: () => {
        return {
            type: tagsTypes.STOP_FETCHING,
        };
    },

    fetchTags: (tags: ITag[]) => {
        return {
            type:    tagsTypes.GET_TAGS,
            payload: tags,
        };
    },

    fetchTagsAsync: (): AppThunk => async (dispatch) => {
        try {
            dispatch(tagsActions.startFetching());

            const tags = await api.getTags();

            dispatch(tagsActions.fetchTags(tags));
        } catch (error) {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const { response } = error as any;
            dispatch(authActions.setError(response.data.message));
            toast.error(response.data.message, {
                position: toast.POSITION.TOP_RIGHT,
            });
        } finally {
            dispatch(taskActions.stopFetching());
        }
    },
});

