import { toast } from 'react-toastify';
import { api } from '../../../api';
import { INewTask, ITask } from '../../../components/types';
import { AppThunk } from '../init/store';
import { taskTypes } from '../types/task';
import { authActions } from './auth';


export const taskActions = Object.freeze({
    setClickedNewTask: (isOpenNewTask: boolean) => {
        return {
            type:    taskTypes.CLICKED_NEW_TASK,
            payload: isOpenNewTask,
        };
    },

    setOpenTask: (task: ITask) => {
        return {
            type:    taskTypes.SET_OPEN_TASK,
            payload: task,
        };
    },

    startFetching: () => {
        return {
            type: taskTypes.START_FETCHING,
        };
    },

    stopFetching: () => {
        return {
            type: taskTypes.STOP_FETCHING,
        };
    },

    fetchTasks: (tasks: ITask[]) => {
        return {
            type:    taskTypes.GET_TASKS,
            payload: tasks,
        };
    },

    createTask: (task: ITask) => {
        return {
            type:    taskTypes.CREATE_TASK,
            payload: task,
        };
    },

    updateTask: (task: ITask) => {
        return {
            type:    taskTypes.UPDATE_TASK,
            payload: task,
        };
    },

    deleteTask: (id: string) => {
        return {
            type:    taskTypes.DELETE_TASK,
            payload: id,
        };
    },

    completeTask: (id: string) => {
        return {
            type:    taskTypes.COMPLETE_TASK,
            payload: id,
        };
    },

    fetchTasksAsync: (): AppThunk => async (dispatch) => {
        try {
            dispatch(taskActions.startFetching());

            const tasks = await api.getTasks();

            dispatch(taskActions.fetchTasks(tasks));
        } catch (error) {
            const { message } = error as Error;
            dispatch(authActions.setError(message));
        } finally {
            dispatch(taskActions.stopFetching());
        }
    },

    // create

    createTaskAsync: (task: INewTask): AppThunk => async (dispatch) => {
        try {
            dispatch(taskActions.startFetching());

            const newTask =  await api.createNewTask(task);

            dispatch(taskActions.createTask(newTask));
            toast.info('Задача добавлена', {
                position: toast.POSITION.TOP_RIGHT,
            });
        } catch (error) {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const { response } = error as any;
            dispatch(authActions.setError(response.data.message));
            toast.error(response.data.message, {
                position: toast.POSITION.TOP_RIGHT,
            });
        } finally {
            dispatch(taskActions.stopFetching());
        }
    },

    // update

    updateTaskAsync: (id: string, task: INewTask): AppThunk => async (dispatch) => {
        try {
            dispatch(taskActions.startFetching());

            const changedTask =  await api.updateTask(id, task);

            dispatch(taskActions.updateTask(changedTask));
            toast.info(`Задача с идентификатором ${id} успешно обновлена`, {
                position: toast.POSITION.TOP_RIGHT,
            });
        } catch (error) {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const { response } = error as any;
            dispatch(authActions.setError(response.data.message));
            toast.error(response.data.message, {
                position: toast.POSITION.TOP_RIGHT,
            });
        } finally {
            dispatch(taskActions.stopFetching());
        }
    },

    // delete

    deleteTaskAsync: (id: string): AppThunk => async (dispatch) => {
        try {
            dispatch(taskActions.startFetching());

            await api.deleteTask(id);

            dispatch(taskActions.deleteTask(id));
            toast.info('Задача удалена', {
                position: toast.POSITION.TOP_RIGHT,
            });
        } catch (error) {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const { response } = error as any;
            dispatch(authActions.setError(response.data.message));
            toast.error(response.data.message, {
                position: toast.POSITION.TOP_RIGHT,
            });
        } finally {
            dispatch(taskActions.stopFetching());
        }
    },
});

