export const tagsTypes = Object.freeze({
    SET_SELECTED_TAG_ID: 'SET_SELECTED_TAG_ID',
    START_FETCHING:      'START_FETCHING',
    STOP_FETCHING:       'STOP_FETCHING',
    GET_TAGS:            'GET_TAGS',
});
