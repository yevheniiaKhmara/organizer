// Core
// import axios from 'axios';

import axios, { AxiosResponse } from 'axios';
import {
    IAuthWithToken, ILoginForm, INewTask, IProfile, ISignUp, ITag, ITask,
} from '../components/types';

export const TODO_API_URL = 'https://lab.lectrum.io/rtx/api/v2/todos';

export const api = Object.freeze({
    getVersion() {
        return '0.0.1';
    },

    get token() {
        return localStorage.getItem('token');
    },

    // auth

    async signup(newUser: ISignUp): Promise<IAuthWithToken> {
        const { data: token } = await axios.post<ISignUp, AxiosResponse<IAuthWithToken>>(
            `${TODO_API_URL}/auth/registration`,
            newUser,
            {
                headers: {
                    'Content-Type': 'application/json',
                },
            },
        );

        return token;
    },

    async login(user: ILoginForm): Promise<IAuthWithToken> {
        const { data: token } = await axios.get<IAuthWithToken>(
            `${TODO_API_URL}/auth/login`,
            {
                headers: {
                    authorization: `Basic ${window.btoa(`${user.email}:${user.password}`)}`,
                },
            },
        );

        return token;
    },

    async logout(): Promise<void> {
        await axios.get(`${TODO_API_URL}/auth/logout`, {
            headers: {
                Authorization: `Bearer ${api.token}`,
            },
        });
    },

    async getProfile(): Promise<IProfile> {
        const { data: profile } = await axios.get<IProfile>(
            `${TODO_API_URL}/auth/profile`, {
                headers: {
                    Authorization: `Bearer ${api.token}`,
                },
            },
        );

        return profile;
    },

    // tags

    async getTags(): Promise<ITag[]> {
        const { data: tags } = await axios.get<ITag[]>(`${TODO_API_URL}/tags`, {
            headers: {
                Authorization:  `Bearer ${api.token}`,
                'Content-Type': 'application/json',
            },
        });

        return tags;
    },

    // tasks

    async createNewTask(newTask: INewTask): Promise<ITask> {
        const { data } = await axios.post<INewTask, AxiosResponse<{ data: ITask }>>(
            `${TODO_API_URL}/tasks`,
            newTask,
            {
                headers: {
                    Authorization:  `Bearer ${api.token}`,
                    'Content-Type': 'application/json',
                },
            },
        );

        return data.data;
    },

    async getTasks(): Promise<ITask[]> {
        const { data: tasks } = await axios.get<AxiosResponse<ITask[]>>(`${TODO_API_URL}/tasks`, {
            headers: {
                Authorization:  `Bearer ${api.token}`,
                'Content-Type': 'application/json',
            },
        });

        return tasks.data;
    },

    async deleteTask(id: string): Promise<ITask> {
        const { data } = await axios.delete<INewTask, AxiosResponse<ITask>>(
            `${TODO_API_URL}/tasks/${id}`,
            {
                headers: {
                    Authorization:  `Bearer ${api.token}`,
                    'Content-Type': 'application/json',
                },
            },
        );

        return data;
    },

    async updateTask(id: string, changedTask: INewTask): Promise<ITask> {
        const { data: task } = await axios.put<AxiosResponse<ITask>>(
            `${TODO_API_URL}/tasks/${id}`,
            changedTask,
            {
                headers: {
                    Authorization:  `Bearer ${api.token}`,
                    'Content-Type': 'application/json',
                },
            },
        );

        return task.data;
    },
});
