// Core
import { FC } from 'react';
import { Routes, Route, Outlet } from 'react-router-dom';
import { ToastContainer, Slide } from 'react-toastify';
import { Footer } from './components/Footer';
import { Navigation } from './components/Navigation';

// Components
import { LoginPage } from './pages/LoginPage';
import { ProfilePage } from './pages/Profile';
import { SignUpPage } from './pages/SignUpPage';
import { TaskManagerPage } from './pages/TaskManagerPage';

// Instruments


export const App: FC = () => {
    return (
        <>
            <ToastContainer newestOnTop transition = { Slide } />
            <Navigation />
            <main>
                <Routes>
                    <Route path = '/todo' element = { <Outlet /> }>
                        <Route path = 'signup' element = { <SignUpPage /> } />
                        <Route path = 'login' element = { <LoginPage /> } />
                        <Route path = 'task-manager' element = { <TaskManagerPage /> } />
                        <Route path = 'profile' element = { <ProfilePage /> } />
                    </Route>

                </Routes>
            </main>
            <Footer />
        </>
    );
};

